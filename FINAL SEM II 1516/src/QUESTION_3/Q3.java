/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_3;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q3 {
    public static void main(String[] args){
        Scanner key = new Scanner(System.in);

        int lines;
       
        do{
            System.out.print("Enter the number of lines : ");
            lines = key.nextInt();
            
            if(lines>0 && lines<16){
                loop(lines);
            }            
        }while(!(lines>0 && lines<16));                
    }
    
    public static void loop(int lines){
        for(int i=1;i<=lines;i++){           
            for(int j=lines-9;j>0;j--){
                if(i>9 && lines-i<j)
                    j = lines-i+1;
                else
                    System.out.print("   ");
            }
                
            for(int j=i;j<9;j++)
                System.out.print("  ");
            
            for(int j=i;j>0;j--){
                if(i==1)
                    System.out.println(j);
                else
                    System.out.print(j + " "); 
            }
                
            for(int j=2;j<=i;j++){
                if(j==i)
                    System.out.println(j);
                else
                    System.out.print(j + " "); 
            }
        }
    }
    
}
