/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_1;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q1 {
    public static void main(String[] args){
        Scanner key = new Scanner(System.in);
        
        double pound, kilo;
        
        System.out.print("Enter a number in pounds: ");
        pound = key.nextDouble();
        
        kilo = pound*0.454;
        
        System.out.println(pound + " pounds is " + kilo + " kilograms");
    }
    
}
