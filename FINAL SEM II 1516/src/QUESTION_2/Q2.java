/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q2 {
    public static void main(String[] args){
        Scanner key = new Scanner(System.in);
        
        int sum=0, temp, num2;
        String num;
        
        do{ 
            
            System.out.print("Enter a number between 0 and 999: ");
            num = key.next();
            num2 = Integer.parseInt(num);
            
            if(num2>0 && num2<1000){
                String temp1;
                int temp2;

                for(int i=0;i<num.length();i++){
                    temp1 = "" + num.charAt(i);
                    temp2 = Integer.parseInt(temp1);
                    sum = sum + temp2;
                }
            }
            
            /*
            temp = num%10;              //932%10 = 2
            sum = sum + temp;           // 0 + 2 = 2
            
            temp = (num/10)&10;         // 932/10 = 93, 93%10=3
            sum = sum + temp;           // 2 + 3 = 5;
                
            temp = num/100;             // 932/100 = 9
            sum = sum + temp;           // 5 + 9 = 14
            */

        }while(!(num2>0 && num2<1000));
        
        System.out.println("The sum of the digits is " + sum);
        
    }
    
}
