/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_5;

import java.util.Random;

/**
 *
 * @author User
 */
public class Q5 {
    public static void main(String[] args){
        Random rand = new Random();
        
        int[][] array = new int[3][3];
        int temp;
        
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                temp = rand.nextInt(2);
                array[i][j] = temp;    
                System.out.print(array[i][j]);
            }
            System.out.println();
        }
        
        //for row
        for(int i=0;i<3;i++){
           if(array[i][0] == 1 && array[i][1] == 1 && array[i][2] == 1){
               System.out.println("All 1s on row " + i);
           } 
        }
        
        //for column
        for(int i=0;i<3;i++){
           if(array[0][i] == 1 && array[1][i] == 1 && array[2][i] == 1){
               System.out.println("All 1s on column " + i);
           } 
        }
        
        //for diagonal
        if(array[0][0] == 1 && array[1][1] == 1 && array[2][2] == 1){
               System.out.println("All 1s on diagonal(s)");
           }
        
        if(array[0][2] == 1 && array[1][1] == 1 && array[2][0] == 1){
               System.out.println("All 1s on diagonal(s)");
           }
        
    }
    
}
