/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q4 {
    
    public static double area(int n,double s){
        double total;
        
        double div = 180/n;
        double rad = Math.toRadians(div);
        double mul = n*s*s;
        double deno = 4*Math.tan(rad);          //to use math.tan, must be in radians O_O
        
        total = (mul/deno);
        
        return total; 
    }
    
    public static void main(String[] args){
        int n;
        double s;
        
        Scanner key = new Scanner(System.in);
        
        System.out.print("Enter the value of n for a regular polygon, n: ");
        n = key.nextInt();
        
        System.out.print("Enter the length of a particular side of the regular polygon (in meter), s: ");
        s = key.nextDouble();
        
        System.out.printf("The area is: %.2f squared meters.",area(n,s));
    }
    
}
