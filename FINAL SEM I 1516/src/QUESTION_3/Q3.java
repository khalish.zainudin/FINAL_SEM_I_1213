/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_3;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner key = new Scanner(System.in);
        
        int num;
        System.out.print("Enter the number of random integer : ");
        num = key.nextInt();
        
        int[] array = new int[num];
        
        Random rand = new Random();
        
        for(int i=0;i<array.length;i++){
            array[i] = rand.nextInt(1001);
        }        
        
        System.out.print("The random integer : ");
        displayArray(array);
        
        maxNum(array);
        
        roundOff(array);
        
        reverseOrder(array);
    }
    
    public static void displayArray(int[] array){  
        for(int i=0;i<array.length;i++){
            System.out.print(array[i] + " ");
        }
    }
    
    public static void maxNum(int[] array){
        System.out.print("\nMaximum number : ");
        
        int max=array[0];
        
        for(int i=1;i<array.length;i++){
            if(array[i]>max)
                max = array[i];
        }
        System.out.print(max);
    }
    
    public static void reverseOrder(int[] array){
        System.out.print("\nThe random integer in reverse order: ");
        
        int temp;
        String tempS;
        
        for(int i=0;i<array.length;i++){
            temp = array[i];
            
            tempS = Integer.toString(temp);
            for(int j=tempS.length()-1;j>=0;j--){
                System.out.print(tempS.charAt(j));
            }
            System.out.print(" ");
        }
    }
    
    public static void roundOff(int[] array2){
        System.out.print("\nThe approximation of the integer to the nearest tenth : ");
        
        int temp, r;
        
        for(int i=0;i<array2.length;i++){
            temp = array2[i];
            
            r = temp%10;
            if(r<5){
                temp = temp-r;
                System.out.print(temp + " ");
            }
            
            else{
                temp = temp-r+10;  
                System.out.print(temp + " ");
            }
        }
    }
    
    
}
