/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_2;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner key = new Scanner(System.in);
        
        int total=0;
        int player,comp;
        int countP=0, countC=0;
        
        do{
            
            do{
                System.out.print("Enter 1.Paper 2.Scissor 3.Rock : ");
                player = key.nextInt();

            }while(!(player>0 || player<4));
            
            System.out.print("Player : " + getChoice(player));
            
            comp = getRandom();
            System.out.println(" ----- Computer : " + getChoice(comp));
           
//******************************************************************************            
            //if(player==1 && comp==1)
            //    System.out.println("Draw!");
            
            if(player==1 && comp==2){
                countC++;
                System.out.println("Computer win " + countC + " time(s)");
            }

            else if(player==1 && comp==3){
                countC++;    
                System.out.println("Computer win " + countC + " time(s)");
            }
            
            else if(player==2 && comp==1){
                countP++;
                System.out.println("Player win " + countP + " time(s)");
            }
            
            //else if(player==2 && comp==2)
            //    System.out.println("Draw!");
                        
            else if(player==2 && comp==3){
                countC++;
                System.out.println("Computer win " + countC + " time(s)");
            }
            
            else if(player==3 && comp==1){
                countP++;
                System.out.println("Player win " + countP + " time(s)");
            }
            
            else if(player==3 && comp==2){
                countP++;
                System.out.println("Player win " + countP + " time(s)");
            }
            
            //else if(player==3 && comp==3)
            //    System.out.println("Draw!");
//******************************************************************************
        }while(!(countC==3 || countP==3));
        
        if(countC==3)
            System.out.println("Computer wins the game.");
        
        else if(countP==3)
            System.out.println("Player wins the game.");
    }
    
    public static int getRandom(){
        Random rand = new Random();
        
        int random;
        random = rand.nextInt(3)+1;
        
        return random;
    }
    
    public static String getChoice(int a){
        String choice=null;
        
        if(a==1){
            choice = "Paper";
        }
        
        
        else if(a==2){
            choice = "Scissor";
        }
        
        else if(a==3){
            choice = "Rock";
        } 
        
        return choice;
    }
    
    
}
