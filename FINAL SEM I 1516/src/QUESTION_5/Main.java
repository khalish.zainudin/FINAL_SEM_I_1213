/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_5;

public class Main {

    public static void main(String[] args) {
        // TODO code application logic here
        
        Rebate r = new Rebate("John Lim","1111222233334444");
        Point p = new Point("John Lim","5555666677778888");
      
        r.setTotalCash(r.getReward());
        p.setTotalCash(p.getReward());
        
        System.out.println(r.toString());
        System.out.println(p.toString());
        
        compare(r,p);
    }
    
    public static void compare(Rebate r,Point p){
        if(r.getReward()>p.getReward())
            System.out.println("The best card is Cash Rebate Card");
        
        else
            System.out.println("The best card is Point Reward Card");
    }
    
}
