/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_5;

public class CreditCard {
    private String name;
    private String cardNum;
    private String cardType;

    public CreditCard(String name, String cardNum, String cardType) {
        this.name = name;
        this.cardNum = cardNum;
        this.cardType = cardType;
    }
 
    private double totalCash;

    public double getTotalCash() {
        return totalCash;
    }

    public void setTotalCash(double totalCash) {
        this.totalCash = totalCash;
    }
    
    @Override
    public String toString(){
        return "Credit Card : " + this.name + " (" + this.cardNum + ")" + "\nCard Type : " + this.cardType + "\nTotal Cash Reward : " + this.totalCash;
    } 
}
