/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_5;

public class Point extends CreditCard {
 
    public Point(String name, String cardNum) {
        super(name, cardNum,"Cash Rebate");
    }
    
    public double getReward(){
        int point;
        double reward;
        
        // Friday = double
        // Saturday = triple
        // Sunday = quadruple
        
        point = (12480/100)*3;                //saturday
        point = point + (6460/100)*2;         //friday
        point = point + (9540/100)*4;         //sunday
        point = point + (10000/100)*2;        //friday
        point = point + (22000/100);          //tuesday
        
        reward = point/100;

        return reward;
    }
    
}
