/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_5;

public class Rebate extends CreditCard {
   
    public Rebate(String name, String cardNum) {
        super(name, cardNum, "Point Reward");
    }
   
    public double getReward(){
        double rebate;
        int type;

  //    System.out.println("    Type         Rebate ");
  //    System.out.println("1.  Fuel           8%   ");
  //    System.out.println("2.  Utility bill   5%   ");
  //    System.out.println("3.  Grocery        2%   ");
  //    System.out.println("4.  Other         0.2%  ");
    
        rebate = 124.80*0.02;
        rebate = rebate + (64.60*0.002);
        rebate = rebate + (95.40*0.08);
        rebate = rebate + (100.00*0.05);
        rebate = rebate + (220.00*0.002);
               
        return rebate;
    }
}
