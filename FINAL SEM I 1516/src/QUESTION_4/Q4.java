/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q4 {
    
    public static void main(String[] args){
        
        String filename = "Q4.txt";
        String temp;
        
        try{
            Scanner in = new Scanner(new FileInputStream(filename));
            
            while(in.hasNextLine()){
                temp = in.nextLine();
                
                System.out.print(temp);
                
                boolean flags = false;
               
                if(length(temp)==true && lowerCase(temp)==true && upperCase(temp)==true && isDigit(temp)==true && otherLetter(temp)==true && space(temp)==true){
                    System.out.println(" - Strong password.");
                }

                else
                    System.out.println(" - NOT a strong password.");
            }
            
            in.close();
        }catch(IOException e){
            System.out.println(e);
        }
    }
    
    public static boolean length(String temp){
        boolean flags=false;
        
        if(temp.length() >= 8)
            flags = true;          
        
        return flags;
    }
    
    public static boolean lowerCase(String temp){
        boolean flags=false;
        
        int count = 0;
        for(int i=0;i<temp.length();i++){
            if(Character.isLowerCase(temp.charAt(i)))
                count++;                   
        }
        if(count>0)
            flags = true;           
                   
        else
            flags = false;
               
        return flags;
    }
    
    public static boolean upperCase(String temp){
        boolean flags=false;
        
        int count = 0;
        for(int i=0;i<temp.length();i++){
            if(Character.isUpperCase(temp.charAt(i)))
                count++;                   
        }
        if(count>0)
            flags = true;           
                   
        else
            flags = false;
               
        return flags;
    }
    
    public static boolean isDigit(String temp){
        boolean flags=false;
        
        int count = 0;
        for(int i=0;i<temp.length();i++){
            if(Character.isDigit(temp.charAt(i)))
                count++;                   
        }
        if(count>0)
            flags = true;           
                   
        else
            flags = false;
               
        return flags;
    }
    
    public static boolean otherLetter(String temp){
        boolean flags=false;
        
        int count = 0;
        for(int i=0;i<temp.length();i++){
            if(!( Character.isLetterOrDigit(temp.charAt(i)) ))
                count++;                  
        }
        if(count>0)
            flags = true;           
                   
        else
            flags = false;
               
        return flags;
    }
    
    public static boolean space(String temp){
        boolean flags = true;

        for(int i=0;i<temp.length();i++){
            if((Character.toString(temp.charAt(i)).equalsIgnoreCase(" ")))
                flags = false;                
        }
               
        return flags;
    }
}
