/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class Staff {
    String name;
    String IC;

    public Staff(String name, String IC) {
        this.name = name;
        this.IC = IC;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIC() {
        return IC;
    }

    public void setIC(String IC) {
        this.IC = IC;
    }
    
 
    public String toString(){
        return "\nFull Name : " + this.name + "\nIC : " + this.IC;
    }
    
}
