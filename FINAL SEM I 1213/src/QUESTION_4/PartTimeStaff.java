/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class PartTimeStaff extends Staff{
    private int hour;
    
    public PartTimeStaff(String name, String IC, int hour) {
        super(name, IC);
        this.hour = hour;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }
    
    public int getSalary(){
        int salary;
        
        salary = this.hour*50;
        
        return salary;
    }
    
    @Override
    public String toString(){         
        return "\nFull Name : " + this.name + "\nIC : " + this.IC + "\nNumber of working hours: " + this.hour + "\nSalary : RM " + getSalary();
    }
}
