/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class PermanentStaff extends Staff {
    private String grade;

    public PermanentStaff(String name, String IC, String grade) {
        super(name, IC);
        this.grade = grade;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
    
    public int getSalary(){
        int salary = 0;
        
        if(this.grade.equalsIgnoreCase("A01"))
            salary = 5000;
        
        else if(this.grade.equalsIgnoreCase("A02"))
            salary = 4000;
        
        else if(this.grade.equalsIgnoreCase("A03"))
            salary = 3000;
        
        else if(this.grade.equalsIgnoreCase("A04"))
            salary = 2000;
        
        return salary;
    }
    
    @Override
    public String toString(){       
        return "\nFull Name : " + this.name + "\nIC : " + this.IC + "\nGrade: " + this.grade + "\nSalary : RM " + getSalary();
    }
    
}
