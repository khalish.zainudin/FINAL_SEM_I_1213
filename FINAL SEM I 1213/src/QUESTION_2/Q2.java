/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q2 {
    public static void main(String[] args){
        Scanner key = new Scanner(System.in);

        String[] name = new String[5];
        
        for(int i=0;i<5;i++){
            System.out.print("Enter student name: ");
            name[i] = key.nextLine();
        }
        
        String temp;
        for(int pass=0;pass<5;pass++){
            for(int i=0;i<4;i++){
                if(name[i].compareToIgnoreCase(name[i+1])>0){
                    temp = name[i];
                    name[i] = name[i+1];
                    name[i+1] = temp;
                }
            }
        }
        System.out.println("The student names in alphabetical order are:");
        for(int i=0;i<5;i++)
            System.out.println(name[i]);
    }
    
}
