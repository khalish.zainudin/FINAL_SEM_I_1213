package LIBRARY;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Scanner;

// @author Khalish
 
public class librarian {
    
    Scanner key = new Scanner(System.in);
    
    private String name;
    
    public librarian() {
        
    }
    
    public void login(){
        int input;
        String name, password;
        
        System.out.println("\n\n # HELLO LIBRARIAN # \n");
        System.out.println("- Login Module - ");
             
        String filename = "";
        String temp;
        boolean flags = false;
        do{      
            System.out.print("Username   : ");
            this.name = key.next();
            System.out.print("Password   : ");
            password = key.next();
            
            filename = "C:/Users/User/Documents/NetBeansProjects/Final Project TXT File/Librarian/" + this.name + ".txt";
            
            try{
                Scanner in = new Scanner(new FileInputStream(filename));
            
                temp = in.nextLine();
                
                if((password).equals(temp)){
                    System.out.println("\n ...*Login Success*...");
                    flags = true;
                    break;
                }                                      
                       
                in.close();
            }catch(IOException e){
               System.out.println("\n ...*Login Failed*...");
               System.out.println("      .Try again.\n");
            }            
        }while(flags==false);
 
        clockIN();
        
        System.out.println("\nWelcome to X Library, " + this.name + ".");
        choice();   
    }
    
    public void choice(){
        int input;
        do{
            System.out.println("\n\nWhat do you want to do?");
            System.out.println("(1)  Add member.");
            System.out.println("(2)  Book Categories (Add/Modify/Delete).");
            System.out.println("(3)  Book Entry in Specific Category (Add/Modify/Delete)");
            System.out.println("(4)  Book Status.");
            System.out.println();
            System.out.println("(0)  Logout.");
            
            System.out.print("\n**Your choice : ");
            input = key.nextInt();
            
            if(input==1){
                addMember();
            }
            
            if(input==2){
                bookCategory();
            }
            
            if(input==3){
                bookEntry();
            }
            
            if(input==4){
                bookStatus();
            }
            
        }while(!(input==0));
        
        if(input==0){
            System.out.println("\n ...You Are Successfully Logged Out!... \n");
            clockOUT();
            return;
        }
    }
    
    public void addMember(){
        String newUser, newPass;
        
        System.out.println("\n\n> Add New Member. <\n ");
        
        System.out.print("Username (must be no spacing) : ");
        newUser = key.next();
        System.out.print("Password                      : ");
        newPass = key.next();
                
        String filename = "C:/Users/User/Documents/NetBeansProjects/Member/" + newUser + ".txt"; 
        try{
            PrintWriter out = new PrintWriter(new FileOutputStream(filename));
            
            out.println(newPass);
            out.println("1 Year Membership");
            
            out.close();
        }catch(IOException e){
            System.out.println(e);
        }
        
        System.out.println("\n ..." + newUser + " was Successfully Added...");
        
    }
/****************************************************************************************/
    public void bookCategory(){
        int input;       
        
        do{
            System.out.println("\n\n> Book Category. <\n ");
        
            System.out.println("(1) Add.");
            System.out.println("(2) Modify.");
            System.out.println("(3) Delete.");
            System.out.println();
            System.out.println("(0) Exit.");
            System.out.print("\n**Your choice : ");
            input = key.nextInt();

            if(input==1){
                addCategory();                    
            }

            if(input==2){
                modifyCategory();
            }

            if(input==3){
                delCategory();
            }           
        }while(!(input==0));
        
    }

    public void addCategory(){
        String newCategory, filename;
        System.out.println("\nNew Book Category.\n");
        System.out.print("Book Category : ");
        newCategory = key.next();
        
        filename = "C:/Users/User/Documents/NetBeansProjects/Final Project TXT File/Book Category/" + newCategory + ".txt";
            
            try{
                PrintWriter out = new PrintWriter(new FileOutputStream(filename));
                                                                                      
                out.close();
            }catch(IOException e){
               System.out.println(e);
            } 
        
        System.out.println("\n ...Book Category '" + newCategory + "' was Successfully Added...");
    }
    
    public void modifyCategory(){
        String newCategory, prevCategory;
        
        System.out.println("\nModify Book Category.\n");
        System.out.print("Previous Book Category : ");
        prevCategory = key.next();
        
        System.out.print("New Book Category      : ");
        newCategory = key.next();
        
        File filenameP = new File("C:/Users/User/Documents/NetBeansProjects/Final Project TXT File/Book Category/"+prevCategory+".txt");
        File filenameN = new File("C:/Users/User/Documents/NetBeansProjects/Final Project TXT File/Book Category/" + newCategory + ".txt");
        
        filenameP.renameTo(filenameN);       
        
        System.out.println("\n ...Book Category '" + prevCategory + "' was Successfully Modified...");
    }
    
    public void delCategory(){
        String delCategory;
        System.out.println("\nDelete Book Category.\n");
        System.out.print("Book Category : ");
        delCategory = key.next();
        
        File fileToDel = new File("C:/Users/User/Documents/NetBeansProjects/Final Project TXT File/Book Category/"+delCategory+".txt");
        fileToDel.delete();
        
        System.out.println("\n ...Book Category '" + delCategory + "' was Successfully Deleted...");
    }
/****************************************************************************************/  
    public void bookEntry(){
        int input;       
        
        do{
            System.out.println("\n\n> Book Entry. <\n ");
 
            System.out.println("(1) Add Book.");
            System.out.println("(2) Modify Book.");
            System.out.println("(3) Delete Book.");
            System.out.println();
            System.out.println("(0) Exit.");
            System.out.print("\n**Your choice : ");
            input = key.nextInt();

            if(input==1){
                addBook();                    
            }

            if(input==2){
               // modifyCategory();
            }

            if(input==3){
               // delCategory();
            }           
        }while(!(input==0));
    }
    
    public void addBook(){
        String newBookID, newBookTitle, bookCategory, filename;
        System.out.println("\nNew Book Entry.\n");
        
        System.out.print("Book Category : ");
        bookCategory = key.next();       
        System.out.print("Book ID    : ");
        newBookID = key.next();
        System.out.print("Book Title : ");
        newBookTitle = key.next();
        
        filename = "C:/Users/User/Documents/NetBeansProjects/Final Project TXT File/Book Category/" + bookCategory + ".txt";
        
        try{
            PrintWriter out = new PrintWriter(new FileOutputStream(filename,true));
            
            out.println(newBookID + "," + newBookTitle + ",Available");
            
            out.close();
        }catch(IOException e){
            System.out.println(e);
        }
            
        
        System.out.println("\n ...Book '" + newBookTitle + "' was Successfully Added in '" + bookCategory + "'...");
    }
    
    public void modifyBook(){
        String newBookID, newBookTitle, prevBookID;
        
        System.out.println("\nModify Book Category.\n");
        System.out.print("Previous Book ID : ");
        prevBookID = key.next();
        
        System.out.print("New Book ID      : ");
        newBookID = key.next();
        
        System.out.print("New Book Title      : ");
        newBookTitle = key.next();
        
        System.out.println("\n ...Book '" + prevBookID + "' was Successfully Modified...");
    }
    
    public void delBook(){
        String delBookID;
        System.out.println("\nDelete Book.\n");
        System.out.print("Book ID : ");
        delBookID = key.next();
        
        System.out.println("\n ...Book '" + delBookID + "' was Successfully Deleted...");
    }
/****************************************************************************************/    
    public void bookStatus(){
        int input;
        String bookID, status;
        
        do{
            System.out.println("\n> Book Status <\n");
            System.out.print("Book ID : ");
            bookID = key.next();
            System.out.print("Status  : ");
            status = key.next();
            
            System.out.println("..." + bookID + " status was updated to " + status + "...");
            
            System.out.println("(1) To continue. (0) Exit.");
            System.out.print("**Your choice : ");
            input = key.nextInt();
        }while(!(input==0));
        
    }
/****************************************************************************************/    
    public void clockIN(){
        try{
            PrintWriter out = new PrintWriter(new FileOutputStream("clockIN.txt",true));
            
            LocalDate dateIN = LocalDate.now();
            LocalTime timeIN = LocalTime.now();
            
            out.println("User Clock-IN : " + this.name);
            out.println("Date          :" + dateIN);
            out.println("Time          :" + timeIN);
            out.println();
            
            out.close();
        }catch(IOException e){
            System.out.println(e);
        }        
    }
    
    public void clockOUT(){
        try{
            PrintWriter out = new PrintWriter(new FileOutputStream("clockOUT.txt",true));
            
            LocalDate dateOUT = LocalDate.now();
            LocalTime timeOUT = LocalTime.now();
            
            out.println("User Clock-OUT : " + this.name);
            out.println("Date           :" + dateOUT);
            out.println("Time           :" + timeOUT);
            out.println();
            
            out.close();
        }catch(IOException e){
            System.out.println(e);
        }        
    }

}

