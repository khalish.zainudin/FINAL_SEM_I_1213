/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

import java.util.Random;

/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Strength a = new Strength("Chaos Knight","Strength",30);
        Agility b = new Agility("Bloodseeker","Agility",25);
        a.display();
        b.display();
        System.out.println();
        playGame(a,b);          

    }
    
    public static void playGame(Strength a,Agility b){
        int life_A = a.getHealth(), life_B = b.getHealth();
        int hit, hit2;
        
        Random rand = new Random();
        
        int turn = rand.nextInt(2);
        
        if(turn==0){
            do{ 
                if(!(life_A<=0 || life_B<=0)){
                    hit = b.damage();
                    life_A=(life_A-hit);
                    System.out.println( b.getName() + " " + hit + " *** " + a.getName() + " (Strength) - HP: " + life_A);

                    System.out.println();

                    hit2 = a.damage();
                    life_B=(life_B-hit2);
                    System.out.println( a.getName() + " " + hit2 + " *** " + b.getName() + " (Agility) - HP: " + life_B);

                    System.out.println();
                }
            }while(!(life_A<=0 || life_B<=0));
            
        }
        
        else{
            do{ 
                if(!(life_A<=0 || life_B<=0)){
                    hit2 = a.damage();
                    life_B=(life_B-hit2);
                    System.out.println( a.getName() + " " + hit2 + " *** " + b.getName() + " (Agility) - HP: " + life_B);

                    System.out.println();

                    hit = b.damage();
                    life_A=(life_A-hit);
                    System.out.println( b.getName() + " " + hit + " *** " + a.getName() + " (Strength) - HP: " + life_A);

                    System.out.println();
                }              
            }while(!(life_A<=0 || life_B<=0));
        }
        
        if(life_A<0)
                System.out.println(b.getName() + " wins the game!");
            
        else
            System.out.println(a.getName() + " wins the game!");
            
        
        
    }
    
}
