/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

import java.util.Random;

/**
 *
 * @author User
 */
public class Agility extends Hero {
    
    public Agility(String name, String type, int health) {
        super(name, type);
        setHealth(health);
    }
    
    public int damage(){
        Random rand = new Random();
        int hit;
        int agility = rand.nextInt(5)+1;
        int skil = rand.nextInt(4);
        
        hit = agility*skil;
        
        return hit;
    }  
}
