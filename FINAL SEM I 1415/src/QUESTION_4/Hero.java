/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class Hero {
    private String name;
    private String type;
    private int health;

    public Hero(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
    
    public void display(){
        System.out.println(this.name + " (" + this.type + ") - HP: " + this.health);
    }
}
