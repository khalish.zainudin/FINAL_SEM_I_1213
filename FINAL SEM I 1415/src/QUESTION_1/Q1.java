/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_1;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q1 {
    
    public static void main (String[] args){
        Scanner in = new Scanner(System.in);
        
        String reverse = "";
        
        System.out.print("Enter a word: ");
        String word = in.next();
        
        for(int i=word.length()-1;i>=0;i--)
            reverse = reverse + word.charAt(i);
        
        System.out.println("Reverse word : " + reverse);
        
        if(word.equalsIgnoreCase(reverse))
            System.out.println("\nThe word is a palindrome.");
        
        else
            System.out.println("\nThe word is not a palindrome.");
    }
    
}
