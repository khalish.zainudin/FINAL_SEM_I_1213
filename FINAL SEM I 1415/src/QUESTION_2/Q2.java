/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Q2 {
    
    public static void main(String[] args){
        Scanner key = new Scanner(System.in);
        int n;
        
        System.out.print("Enter number of tender : ");
        n = key.nextInt();
        
        String[] name = new String[n];
        double[] price = new double[n];
        
        for(int i=0;i<n;i++){
            System.out.print("Enter name : ");
            key.nextLine();
            name[i] = key.nextLine();
            
            System.out.print("Enter reservation price : ");
            price[i] = key.nextDouble();
        }
        
        double temp;
        String temp2;
        for(int i=0;i<n;i++){
            for(int j=1;j<n-1;j++){
                if(price[j]>price[j-1]){
                    temp = price[j];
                    temp2 = name[j];
                    price[j] = price[j-1];
                    name[j] = name[j-1];
                    price[j-1] = temp;
                    name[j-1] = temp2;
                }                  
            }
        }
        System.out.println("List of tender : ");
        
        for(int i=0;i<n;i++){
            System.out.print(name[i]);
            
            if(i<n-1)
                System.out.print(", ");
        }
        
        System.out.printf("\nThe winner of BMW88 : %s (%.2f)",name[0], price[0]);
    }
    
}
