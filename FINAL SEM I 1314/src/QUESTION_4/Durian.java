/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class Durian extends Fruit{
    private double weight;
    private String type;
    private double total;

    public Durian(String name, String type, double weight) {
        super(name, type);
        this.weight = weight;
        this.type = type;
    }
    
    public double totalPrice(){
        if(this.type.equalsIgnoreCase("Musang King"))
            this.total = this.weight*28;
        
        else if(this.type.equalsIgnoreCase("Udang Merah"))
            this.total = this.weight*25;
        
        else if(this.type.equalsIgnoreCase("D24"))
            this.total = this.weight*22;
        
        else
            this.total = this.weight*18;
        
        return this.total;
    }
    
    public void display(){
        display2();     
        System.out.printf("%.1f KG - RM%.2f",this.weight,totalPrice());
    }
    
    
}
