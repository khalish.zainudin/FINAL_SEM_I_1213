/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Apple a = new Apple("Apple","Green",20.3);
        Apple b = new Apple("Apple","Red",10.8);
        Durian c = new Durian("Durian","D24",3.2);
        Durian d = new Durian("Durian","Musang King",5.9);
        
        a.display();
        c.display();
        comparePrice(a,c);
        
        b.display();
        d.display();
        comparePrice(b,d);
    }
    
    public static void comparePrice(Apple a, Durian b){
        if(a.totalPrice()>b.totalPrice())
            System.out.print("\n" + a.getType() + " " + a.getName() + " is more expensive than " + b.getType() + " " + b.getName());
    
        else
            System.out.print("\n" + b.getType() + " " + b.getName() + " is more expensive than " + a.getType() + " " + a.getName());
    }
    
}
