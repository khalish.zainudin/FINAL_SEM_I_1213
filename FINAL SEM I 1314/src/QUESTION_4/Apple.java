/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class Apple extends Fruit{    
    private double weight;
    private String type;
    private double total;

    public Apple(String name, String type, double weight) {
        super(name, type);
        this.weight = weight;
        this.type = type;
    }
    
    public double totalPrice(){
        
        if(this.type.equalsIgnoreCase("green")){
            this.total = this.weight*7.2;
        }
        
        else{
            this.total = this.weight*8.3;
        }
        
        return this.total;
    }
    
    public void display(){
        display2();
        System.out.printf("%.1f KG - RM%.2f",this.weight,totalPrice());
    }
    
}
