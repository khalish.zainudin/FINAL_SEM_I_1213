/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_4;

/**
 *
 * @author User
 */
public class Fruit {
    private String name;
    private String type;

    public Fruit(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
    
    public void display2(){
        System.out.println();
        System.out.println(this.type + " " + this.name);
    }
    
}
