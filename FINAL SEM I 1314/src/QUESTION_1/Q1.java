/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QUESTION_1;

import java.util.Random;

/**
 *
 * @author User
 */
public class Q1 {
    
    public static void main(String[] args){
        Random r = new Random();
        
        int count = 0;
        int num;
        
        boolean flags = false;
        
        while(true){
            num = r.nextInt(20)+1;
            
            if(num%2==0){
                System.out.println("Even number: Exit While Loop.");
                break;
            }
            
            else{
                count++;
                System.out.println(num);
                flags = true;
            }
        }
        
        System.out.println("\nThe total number of random odd number from 1-20 is " + count);
        
    }
    
}
