/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class T4_Q1_c {
    
    public static void main(String[] args) {
        
        Scanner key = new Scanner(System.in);
        
        int i,fact;  
        int number;                     //It is the number to calculate factorial    
        
        fact = 1;
        
        System.out.print("Enter value for n : ");
        number = key.nextInt();
        
        for(i=1;i<=number;i++){    
            fact=fact*i;    
        } 
        
        System.out.println("Factorial of " + number + " is: " + fact);    
        
    }
    
}
