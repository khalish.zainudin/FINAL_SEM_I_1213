/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

/**
 *
 * @author User
 */
public class T7_Q1_d {
    
    public static void main(String[] args)throws FileNotFoundException, IOException{
        
        String filename = "integer2.dat";
        
        //read a binary file
        ObjectInputStream output = new ObjectInputStream(new FileInputStream(filename));
        
        //process for the binary file
        //storng the binary file
        int[] integer = new int[10];
        float sum = 0;
        
        for(int i=0;i<10;i++){
            integer[i] = output.readInt();
            sum = sum + integer[i];
        }
        
        float ave = sum/10;
        
        //display
        for(int j=0;j<10;j++){
            System.out.println(integer[j]);                       
        }
        
        System.out.println("\nThe average is " + ave);
        
        output.close();
        
    }
    
}
