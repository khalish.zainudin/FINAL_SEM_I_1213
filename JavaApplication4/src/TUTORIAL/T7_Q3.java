/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;



/**
 *
 * @author User
 */
public class T7_Q3 {
    
    public static void main(String[] args) throws IOException{
        
        String filename = "course.dat";
//        String filename2 = "course.txt";
        
        ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(filename));
//        PrintWriter output2 = new PrintWriter(new FileOutputStream(filename2));
        
        String title = "Course Code     Course Name";
        output.writeUTF(title);        
//        output2.write(title);
//        output2.println();
        
        String a = "WXES1116        Programming I";
        output.writeUTF(a);
//        output2.write(a);
//        output2.println();
        
        String b = "WXES1115        Data Structure";
        output.writeUTF(b);
//        output2.write(b);
//        output2.println();
        
        String c = "WXES1110        Operating System";
        output.writeUTF(c);
//        output2.write(c);
//        output2.println();
        
        String d = "WXES1112        Computing Mathematics I";
        output.writeUTF(d);
//        output2.write(d);
//        output2.println();
        
        
        output.close();
//        output2.close();
    }
    
}
