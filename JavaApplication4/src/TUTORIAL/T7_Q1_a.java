/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;

public class T7_Q1_a {
    
    public static void main(String[] args) {
        
        PrintWriter output = null;
        String filename = "integer.txt";
        
        try{
            output = new PrintWriter(filename);            
        }
        catch(FileNotFoundException e){
            System.out.println("ERROR ! File not found.");
            System.exit(0);
        }
        
        Random rand = new Random();
        
        for(int i=1;i<=10;i++)
            output.println(rand.nextInt(1001));
        
        output.close();
        
    }
    
}
