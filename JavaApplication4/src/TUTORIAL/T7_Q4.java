/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class T7_Q4 {
    
    public static void main(String[] args) throws IOException{
        
        String filename = "course.dat";
        
        ObjectInputStream input = new ObjectInputStream(new FileInputStream(filename));
        
        String[][] data = new String[5][2];
        
        Scanner key = new Scanner(System.in);
       
        System.out.print("Enter course code: ");
        String code = key.next();
        
        int i, j;
        for(i=0;i<5;){
            String a = input.readUTF();
            if(code.equalsIgnoreCase(a))
                System.out.println(a);
            
            else
                i++;
            
        }
        
    }
    
}
