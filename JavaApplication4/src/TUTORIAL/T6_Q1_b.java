/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

/**
 *
 * @author User
 */
public class T6_Q1_b {
    
    public static void sqrt_num(double a){ //to determine square number

        double sqr = Math.sqrt(a);  //if its a square number, it will give non-decimal number
        double x;
        
        x=sqr-Math.floor(sqr);
        
        System.out.println("math.floor " + Math.floor(sqr));
        
        if(x>0)
            System.out.printf("NO. %.0f is not a square number.",a);       
        else
            System.out.printf("YES. %.0f is a square number.",a);  
    }
    
    public static void main(String[] args){
        
        double a = 8;
        
        sqrt_num(a);
        
        
    }
    
}
