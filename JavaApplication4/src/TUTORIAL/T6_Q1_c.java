/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

/**
 *
 * @author User
 */
public class T6_Q1_c {
    
    public static int fac(int a){
        int fact=1;
        for(int i=1;i<=a;i++){
            fact=fact*i;
        }        
        return fact;
    }
    
    public static int C(int a, int b, int c){        
        int sum = a/(b*c);
        return sum;
    }
    
    public static void main(String[] args){        
        int n=9,k=6;

        System.out.println("Factorial of " + n + " is " + fac(n));
        System.out.println("Factorial of " + k + " is " + fac(k));
        System.out.println("Factorial of " + n + "-" + k + " is " + fac(n-k));
        System.out.println("\nThe answer for C is " + C(fac(n), fac(k), fac(n-k)));
        
    }
    
}
