/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class T7_Q1_b {
    
    public static void main(String[] args){
        
        //read from filr
                
        try{
            
            Scanner input = new Scanner(new FileInputStream("integer.txt"));
            
            int num =0, max=0;
            
            System.out.println("List of integer:");
            
            while(input.hasNextInt()){
                num = input.nextInt();
                System.out.print(num + " ");
                if(num>max)
                    max = num;
            }
            
            System.out.println();
            System.out.println("Largest int = " + max);

        
        input.close();   
            
            
        }catch(FileNotFoundException e){
            System.out.println("File was not found");
        }
        
        
    }
    
}
