/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class T5_Q4 {
    
    public static void main(String[] args){
        
        Scanner input = new Scanner(System.in);
        
        String[] array = new String[11];
        
        System.out.print("Enter a sentence (less than 12 words): ");
        int i;
        for(i=0;i<array.length;i++){
            
            array[i] = input.nextLine();
            
        }
        
        System.out.print("\nTHE SENTENCE: ");
        
        int n=0;
        for(i=0;i<array.length;i++){
            
            System.out.print(array[i] + " ");
            
            if(array[i].equalsIgnoreCase("the")){
                n++;                 
            }
        }
        
        System.out.println("\n\nThe number of 'the' is: " + n);
        
    }
    
}
