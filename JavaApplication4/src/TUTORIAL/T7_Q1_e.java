/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 *
 * @author User
 */
public class T7_Q1_e {
    
    public static void main(String[] args) throws FileNotFoundException{
        
        String filename = "matrix.txt";
        
        PrintWriter output =  new PrintWriter(new FileOutputStream(filename));
        
        int[][] matrix = {{8,10,4,9},{9,2,5,2},{6,7,2,6}};
        
        for(int i=0;i<3;i++){
            for(int j=0;j<4;j++){
                output.print(matrix[i][j] + " ");
            }
            output.println();
        }
        
        
        output.close();
    }
    
}
