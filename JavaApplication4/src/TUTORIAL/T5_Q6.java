/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

/**
 *
 * @author User
 */
public class T5_Q6 {
    
    public static void main(String[] args){
        
        int[][] matrix = {{1,2,3,4},{3,4,5,6},{5,6,7,8},{7,8,9,8},{9,8,7,6},{1,2,3,4}};
        
        int i;
        
        for(i=0;i<6;i++){
            for(int j=0;j<4;j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();            
        }
        int total=0;
        for(i=0;i<6;i++){
            for(int j=0;j<4;j++){
                if(matrix[i][j]%2==0)
                    total++;
            }
        }
        
        System.out.println("\n\nThe number of even number in an matrix above is " + total);
    }
    
}
