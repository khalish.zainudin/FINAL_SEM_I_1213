/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL;

import java.util.Random;

/**
 *
 * @author User
 */
public class T6_Q1_a {
    
    public static int max_num(int a, int b, int c){
        
        int max=0;
        
        if(a>max)
            max = a;
        
        if(b>max)
            max = b;
        
        if(c>max)
            max = c;
        
        return max;    
    }
    
    public static void main(String[] args){
        
        Random rand = new Random();
        
        int a, b, c;
        
        a=rand.nextInt(100);
        b=rand.nextInt(100);
        c=rand.nextInt(100);
        
        System.out.println("The max number of " + a + ", " + b + " and " + c + " is " + max_num(a, b, c));
        
    }
    
}
