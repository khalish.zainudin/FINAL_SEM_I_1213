/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q3;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class array {
    
    public int num;
    public int[] numArray;

    public array() {
        Scanner key = new Scanner(System.in);
        
        System.out.print("Enter a parameter for an array : ");
        this.num = key.nextInt();
    }

    public array(int num) {
        this.num = num;
    }

    public void getRandom(){
        Random rand = new Random();
        
        this.numArray = new int[this.num];
        
        for(int i=0;i<this.numArray.length;i++)
            this.numArray[i] = rand.nextInt(100)+1;       
    }
    
    public void sortRandomInc(){
        System.out.println("\n\nSorting array in ascending order.");
        
        int temp;
        
        for(int i=0;i<this.numArray.length;i++){
            for(int j=0;j<this.numArray.length-1;j++){
                if(this.numArray[j]>this.numArray[j+1]){
                    temp = this.numArray[j];
                    this.numArray[j] = this.numArray[j+1];
                    this.numArray[j+1] = temp;                    
                }
            }
        }    
    }
    
    public void sortRandomDec(){
        System.out.println("\n\nSorting array in descending order.");
        
        int temp;
        
        for(int i=0;i<this.numArray.length;i++){
            for(int j=0;j<this.numArray.length-1;j++){
                if(this.numArray[j]<this.numArray[j+1]){
                    temp = this.numArray[j];
                    this.numArray[j] = this.numArray[j+1];
                    this.numArray[j+1] = temp;                    
                }
            }
        }    
    }
    
    public void displayRandom(){
        for(int i=0;i<this.numArray.length;i++)
            System.out.print(this.numArray[i] + " ");        
    }
}
