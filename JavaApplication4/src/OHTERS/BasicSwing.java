/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package OHTERS;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BasicSwing extends JFrame {
    
    JPanel p = new JPanel();
    JButton b = new JButton("OK");
        
//    JTextField t = new JTextField("    ",5); //kotak utk tulis tp xbergerak
//    JTextArea ta = new JTextArea("     ",2,5); //kotak utk tulis tp bergerak
    JLabel l = new JLabel("Please make your choice \n");
    
    String choices[] = {"salam", "hi", "hello"};
    JComboBox co = new JComboBox(choices);
    
    public static void main(String[] args){       
        new BasicSwing();        
    }
    
    public BasicSwing(){
        super("Basic Swing App");
        
        setSize(150,400);       //size utk app
        setResizable(true);
        
        p.add(l);
        p.add(co);
        p.add(b);          
        add(p); 
     
        setVisible(true);
        }
    
}
