/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q5;

import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class payment {
    
    public int choice;

    public payment() {
        Scanner key = new Scanner(System.in);
        
        getAmount();
        
        System.out.println("\n\n");
        
        System.out.println("~ PAYMENT METHOD ~");
        System.out.println("1. Cash Payment");
        System.out.println("2. Cheque Payment");
        System.out.println("3. Credit Card Payment");
        
        do{
            System.out.print("Choose your payment method [1-3] : ");
            this.choice = key.nextInt();           
        }while(!(this.choice>=1 && this.choice<=3));
  
        if(this.choice == 1)
            cash();
        
        if(this.choice == 2)
            cheque();
        
        if(this.choice == 3)
            creditCard();
    }
        
    public void cash(){
        Scanner key = new Scanner(System.in);
        
        int amount;
        
        System.out.print("Enter amount of cash: RM");
        amount = key.nextInt();
        
    }
    
    public void cheque(){
        Scanner key = new Scanner(System.in);
        
        int amount;
        BigInteger chequeNum;
        
        System.out.print("Enter amount          : RM");
        amount = key.nextInt();
        
        System.out.print("Enter cheque number : ");
        chequeNum = key.nextBigInteger();
        
    }
    
    public void creditCard(){
        Scanner key = new Scanner(System.in);
        
        int amount, expireDate;
        String name, cardType, validCode;
        
        System.out.print("Enter amount                  : RM");
        amount = key.nextInt();
        
        System.out.print("Enter the card holder name    : ");
        name = key.next();
        
        System.out.print("Enter the type of card        : ");
        cardType = key.next();
        
        System.out.print("Enter the expired date (MMYY) : ");
        expireDate = key.nextInt();
        
        System.out.print("Enter the validation code     : ");
        validCode = key.next();
    }
    
    public void getAmount(){
        Random rand = new Random();
        
        int amount = 1+rand.nextInt(1000);
        float RM = (float)(amount);
        System.out.printf("The amount of your payment is : RM%.2f",RM);
        
    }
    
}
