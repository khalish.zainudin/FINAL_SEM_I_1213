/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

/**
 *
 * @author User
 */

// degree Fahrenheit to degree celcius

import java.util.Scanner;                                    //scanner must be before class

public class Q1 {
      
     public static void main (String[] args){
                
         Scanner keyboard = new Scanner(System.in);         
         
         double celsius;
         double fahrenheit;
         
         System.out.print("Please enter Sthe temperature in degree fahrenheit: ");
         
         fahrenheit = keyboard.nextDouble();                //scanf
         
         celsius = ((fahrenheit - 32) / 1.8);               //compute
         
         System.out.printf("%.2f \n", celsius);
                  
     }
}
