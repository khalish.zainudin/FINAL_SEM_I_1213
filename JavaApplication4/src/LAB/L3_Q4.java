/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class L3_Q4 {
    
    public static void main (String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        double commission, sale;
        commission = 0;
        
        System.out.println("HELLO!! \nThis program will calculate the amount of commission you will get for a certain sales volume.");
        
        System.out.print("\nPlease enter the amount of sales volume: RM");
        sale = keyboard.nextDouble();
        
        if (sale <= 100) 
            commission = 5 * sale / 100;
        
        else if (sale > 100 && sale <=500)
            commission = 7.5 * sale / 100;
                       
        else if (sale > 500 && sale <=1000)
            commission = 10 * sale / 100;
        
        else if (sale > 1000)
            commission = 12.5 * sale / 100;
        
        System.out.printf("\n\nThe amount of commision you will get is RM%.2f", commission);
        
    }
          
}
