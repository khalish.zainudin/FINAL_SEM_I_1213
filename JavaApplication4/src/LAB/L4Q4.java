/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author User
 */
public class L4Q4 {
    
    public static void main(String[] args){
        
        Random rand = new Random();
        Scanner input = new Scanner(System.in);
        
        int p_1=0, p_2=0;
        int sum_p1=0, sum_p2=0;
        
        System.out.println("HELLO ! WELCOME TO DICE GAME !!\n");
        System.out.println("PLAYER 1 will start first.\n");
        
        for(int i=1; ;i++){
                                  
            p_1 = input.nextInt();
            System.out.println("Player 1 got " + p_1 + "\n");
            sum_p1 = sum_p1 + p_1;
                        
            if(p_1==6){
                
                do{
                    System.out.println("ROLL AGAIN ! \n");
                    p_1 = input.nextInt();
                    System.out.println("Player 1 got " + p_1 + "\n");  
                    sum_p1 = sum_p1 + p_1;                                           
                }while(p_1==6);                                
            }
            
            //if(p_1!=6){
                
                System.out.println("\nPLAYER 2's turn.\n");
                //break;
            //}
                        
            p_2 = input.nextInt();
            System.out.println("Player 2 got " + p_2 + "\n");
            sum_p2 = sum_p2 + p_2;
            
            if(p_2==6){
                System.out.println("ROLL AGAIN ! \n");
                p_2 = input.nextInt();
                System.out.println("Player 2 got " + p_2 + "\n");  
                sum_p2 = sum_p2 + p_2;   
            }
            
            if(p_2!=6){
                
                System.out.println("\nPLAYER 1's turn.\n");
                //continue;
            }
            
            if(sum_p2 >= 100 || sum_p1 >= 100)
                break;
        }
        
        System.out.println("Player 1's score is " + sum_p1);
        System.out.println("Player 2's score is " + sum_p2);
        
        if(sum_p1 > sum_p2)
            System.out.println("\nPLAYER 1 WINS !!!");
        
        else
            System.out.println("\nPLAYER 2 WINS !!!");
        
        System.out.println("\n\n! GAME OVER !");
    }
    
}
