/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

/**
 *
 * @author User
 */

import java.util.Scanner;

public class Q2 {
    
    public static void main (String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        double P, D, R, Y, M;
        
        System.out.println("!!! HELLO !!! \nWELCOME TO THE CAR LOAN CALCULATOR.  \n");
        
        System.out.print("Price of the car        : ");
        P = keyboard.nextDouble(); 
        
        System.out.print("Downpayment             : ");
        D = keyboard.nextDouble(); 
        
        System.out.print("Interest Rate in %      : ");
        R = keyboard.nextDouble(); 
        
        System.out.print("Loan Duration in Year   : ");
        Y = keyboard.nextDouble(); 
        
        M = (P-D) * (1 + R * Y / 100) / (Y * 12);
        
        System.out.println("\n************************************************");
        System.out.printf("The Monthly Payment for car loan is RM%.2f \n", M);
        System.out.println("************************************************");
    }
    
}
