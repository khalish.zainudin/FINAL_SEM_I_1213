/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class L3_Q2 {
    
    public static void main(String[] args) {
        
        Scanner reader = new Scanner(System.in);
        Scanner keyboard = new Scanner(System.in);
        
        int a, b, ans;
        char c;
        
        ans = 0;
        
        System.out.print("\nEnter the first Integer         : ");
        a = keyboard.nextInt();
        
        System.out.print("\nEnter a character (+, -, x, /)  : ");
        c = reader.findInLine(".").charAt(0);
                
        System.out.print("\nEnter the second Integer        : ");
        b = keyboard.nextInt();
        
        System.out.printf("%d %c %d = ??? \n", a, c, b);
        
        System.out.println("\n-----Press ENTER to continue...-----");
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {} 
           
        System.out.println("CALCULATING.... \n.\n.\n.");
        
        switch (c) {
            
            case 'x':
                ans = a * b ;
                break;
            
            case '+':
                ans = a + b ;
                break;    
            
            case '-':
                ans = a - b ;
                break;   
                
            case '/':
                ans = a / b ;
                break;    
                
            }
            
        System.out.printf("\n%d %c %d = %d \n", a, c, b, ans);
    }
    
}
