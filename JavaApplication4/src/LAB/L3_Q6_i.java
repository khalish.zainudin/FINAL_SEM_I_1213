/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class L3_Q6_i {
    
    public static void main (String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        String type;
        double hour, salary;
        salary = 0;
        
        System.out.println("HELLO EMPLOYEE !!!");
        System.out.println("\nWhat kind of employee you are?\n");
        System.out.println("\t1. Permanent Staff - P ");
        System.out.println("\t2. Contract Staff  - C ");
        System.out.println("\t3. Temporary Staff - T ");
        
        System.out.print("\nPlease choose one of those type by entering the keyword on the right : ");
        type = keyboard.nextLine();
        
        System.out.print("\nPlease enter the numbers of hours you had been worked: ");
        hour = keyboard.nextDouble();
        
        if (type.equalsIgnoreCase("P"))
            salary = hour * 200;
        
        else if (type.equalsIgnoreCase("C"))
            salary = hour * 150;
        
        else if (type.equalsIgnoreCase("T"))
            salary = hour * 100;
        
        else
            System.out.println("404 ERROR !!! YOU ENTER WRONG KEYWORD !");
                                
        System.out.printf("\nYour salary is RM%.2f \n", salary);
       
    }
    
}
