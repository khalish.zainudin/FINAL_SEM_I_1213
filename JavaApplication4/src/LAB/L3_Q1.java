/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class L3_Q1 {
    
    public static void main(String[] args ) {
        
        Scanner keyboard = new Scanner(System.in);
        
        int num;
        
        System.out.println("HELLO KIDS !");
        System.out.println("\nPLEASE CHOOSE A NUMBER FROM THE FOLLOWING LIST: \n");
        System.out.println("    1. Square");
        System.out.println("    2. Circle");
        System.out.println("    3. Triangle");
        System.out.println("    4. Rectangle");
        
        System.out.print("\nNUMBER = ");
        num = keyboard.nextInt();
            
        switch (num) {
            
            case 1:
                System.out.println("\n");
                System.out.println(" * * * * ");
                System.out.println(" *     * ");
                System.out.println(" *     * ");
                System.out.println(" * * * * ");
                break;
                
            case 2:
                System.out.println("\n");
                System.out.println("    ***     ");
                System.out.println("  *     *   ");
                System.out.println(" *       *  ");
                System.out.println("  *     *   ");
                System.out.println("    ***     ");
                break;
                
            case 3:
                System.out.println("\n");
                System.out.println("     *      ");
                System.out.println("    * *     ");
                System.out.println("   *   *    ");
                System.out.println("  *     *   ");
                System.out.println(" * * * * *  ");
                break;
                
            case 4:
                System.out.println("\n");
                System.out.println(" * * * * * * * ");
                System.out.println(" *           * ");
                System.out.println(" *           * ");
                System.out.println(" *           * ");
                System.out.println(" * * * * * * * ");
                break;    
                
                
        }
        
    }
    
}
