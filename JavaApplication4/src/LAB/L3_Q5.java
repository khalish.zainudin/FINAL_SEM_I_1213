/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LAB;

import java.util.Random;

/**
 *
 * @author User
 */
public class L3_Q5 {
    
    public static void main (String[] args) {
        
        Random rand = new Random();
        
        int P1_1, P2_1, P1_2, P2_2, sum_1, sum_2;
        
    
    //**************************************************************************    
        System.out.print("\nPress enter for first dice roll for PLAYER ONE...");
        
    //-------------------------------------------------------------------------           
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {}
    //--------------------------------------------------------------------------  
    
        P1_1 = rand.nextInt(7);
        
        System.out.println("\tPLAYER ONE'S FIRST ROLL GOT : " + P1_1);
        
        
    //**************************************************************************           
        System.out.print("\nPress enter for first dice roll for PLAYER TWO...");
        
     //-------------------------------------------------------------------------           
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {}
    //--------------------------------------------------------------------------  
    
        P2_1 = rand.nextInt(7);
        
        System.out.println("\tPLAYER TWO'S FIRST ROLL GOT : " + P2_1);
    
        
    //**************************************************************************    
        System.out.print("\nPress enter for second dice roll for PLAYER ONE...");
        
     //-------------------------------------------------------------------------           
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {}
    //--------------------------------------------------------------------------  
    
        P1_2 = rand.nextInt(7);
        
        System.out.println("\tPLAYER ONE'S SECOND ROLL GOT : " + P1_2);
        
        
    //**************************************************************************    
        System.out.print("\nPress enter for second dice roll for PLAYER TWO...");
        
     //-------------------------------------------------------------------------           
        try
        {
            System.in.read();
        }  
        catch(Exception e)
        {}
    //--------------------------------------------------------------------------  
    
        P2_2 = rand.nextInt(7);
        
        System.out.println("\tPLAYER TWO'S SECOND ROLL GOT : " + P2_2);
        
        sum_1 = P1_1 + P1_2;
        sum_2 = P2_1 + P2_2;
        
        System.out.println("\n\n***********************************************");
        System.out.println("Total marks for PLAYER ONE is " + sum_1);
        System.out.println("Total marks for PLAYER TWO is " + sum_2);
        
        if (sum_1 > sum_2)
            System.out.println("\n\t !!! PLAYER ONE WINS !!!");
        
        else
            System.out.println("\n\t !!! PLAYER TWO WINS !!!");
        
        System.out.println("\n\t*** GAME OVER ***");
        
    }
      
}

