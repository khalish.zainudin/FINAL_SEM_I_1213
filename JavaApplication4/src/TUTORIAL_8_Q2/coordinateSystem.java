package TUTORIAL_8_Q2;


import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class coordinateSystem {

    public int valueX, valueY;

    public coordinateSystem() {
        insertValue();       
    }
/*
    public int getValueX() {
        return valueX;
    }

    public void setValueX(int valueX) {
        this.valueX = valueX;
    }

    public int getValueY() {
        return valueY;
    }

    public void setValueY(int valueY) {
        this.valueY = valueY;
    }
*/
    
    public coordinateSystem(int valueX, int valueY) {
        this.valueX = valueX;
        this.valueY = valueY;
    }

    public void insertValue(){
        Scanner key = new Scanner(System.in);
        
        System.out.print("Enter value for X: ");
        this.valueX = key.nextInt();
        System.out.print("Enter value for Y: ");
        this.valueY = key.nextInt();        
    }
    
/*
    public int getValueX() {
        return valueX;
    }

    public void setValueX(int valueX) {
        this.valueX = valueX;
    }

    public int getValueY() {
        return valueY;
    }

    public void setValueY(int valueY) {
        this.valueY = valueY;
    }
*/    
    
    public void displayCoordinate(){        
        System.out.println("The coordinate is (" + this.valueX + "," + this.valueY + ").");        
    }
    
}
