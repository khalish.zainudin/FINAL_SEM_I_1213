/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q4;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class digitsMultip {
    
    public int integer;

    public digitsMultip() {
        Scanner key = new Scanner(System.in);
        
        System.out.print("Enter a positive integer: ");
        this.integer = key.nextInt();
    }

    public digitsMultip(int integer) {
        this.integer = integer;
    }
    
    public void total2(){
        String A;
        
        A = Integer.toString(this.integer);
        
        String a;
        int b;
        int sum = 1;       
        
        for(int i=0;i<A.length();i++){
            a = "" + A.charAt(i);
            b = Integer.parseInt(a);
            
            sum = sum*b;
        }
        System.out.println("The Multiplication of the digits is : " + sum);
    }
    
}
