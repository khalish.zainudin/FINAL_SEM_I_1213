/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q4;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class digitMultiplication {

    public int n, sum;
    public int[] positiveInt;
    
    public digitMultiplication() {
        Scanner key = new Scanner(System.in);
        
        System.out.print("Enter the length of your positive integer: ");
        this.n = key.nextInt();
    }
    
    public digitMultiplication(int n) {
        this.n = n;      
    }

    public void getDigits(){
        Scanner key = new Scanner(System.in);
        
        int temp;
        
        this.positiveInt = new int[this.n];
        
        System.out.print("Enter each of the integer with a space: ");
        
        int i=0;
        while(i<this.positiveInt.length){           
            temp = key.nextInt();
            if(temp<10){
                this.positiveInt[i] = temp;
                i++;
            }         
        }
        
    //    for(i=0;i<this.positiveInt.length;i++)
    //        System.out.print(this.positiveInt[i]);
    }
    
    public void getTotal(){
        this.sum = 1;
        
        for(int i=0;i<this.positiveInt.length;i++){
            this.sum = this.sum*this.positiveInt[i];
        }
        
        System.out.println("\n\nThe total multiplication of the digits is : " + this.sum);
    }
    
}
