/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q4;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class digitMulti {
    
    public String integer;

    public digitMulti() {
        Scanner key = new Scanner(System.in);
        
        System.out.print("Enter a positive integer: ");
        this.integer = key.next();
    }

    public digitMulti(String integer) {
        this.integer = integer;
    }
    
    public void total(){
        String a;
        int b;
        int sum = 1;       
        
        for(int i=0;i<this.integer.length();i++){
            a = "" + this.integer.charAt(i);
            b = Integer.parseInt(a);
            
            sum = sum*b;
        }
        System.out.println("The Multiplication of the digits is : " + sum);
    }
}
