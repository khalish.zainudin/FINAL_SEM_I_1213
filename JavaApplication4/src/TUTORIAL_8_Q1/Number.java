/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q1;

/**
 *
 * @author User
 */
public class Number {
    
    public int value;

    public Number() {
        this.value = 0;
    }
   
    public Number(int value) {
        this.value = value;
    }
    
    public void displayNum(){
        System.out.println("The number is " + this.value);
    }
    
}
