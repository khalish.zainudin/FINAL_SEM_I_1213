/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TUTORIAL_8_Q1;

/**
 *
 * @author User
 */

//a class student
public class Student {

    public int contactNum;                         //b)instance variable

    public Student() {                              //c)constructor set to null
        this.contactNum = 0;
    }

    public Student(int contactNum) {               //d)constructor assign parameter value
        this.contactNum = contactNum;
    }
    
    /*
    public int getContactNum() {                   //e)ancessor method
        return contactNum;
    }

    public void setContactNum(int contactNum) {   //e)mutator method
        this.contactNum = contactNum;
    }
    */

    public void displayContactNumber(){                             //f)method to display       
        System.out.println("Contact number: " + this.contactNum);       
    }

    public void setContactNum(int contactNum) {                     //h)change using mutator
        this.contactNum = 12345;
    }
    
    
}
